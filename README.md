**Mais fácil que isso impossível**

1. Clone o projeto;
2. Instale o docker;
3. Rode o comando: "sudo docker build -t desafio . ; sudo docker run -d --name DESAFIO --restart=always desafio";
4. Acesse: "localhost:5000" no browser mais próximo de você;
5. Para copiar o arquivo csv do container para a máquina basta usar o comando: "docker cp {CONTAINER_ID}:{FILENAME} result.csv"; PS: substituir variáveis CONTAINER_ID e FILENAME por, respectivamente, id do container docker e nome do arquivo no final da pesquisa.
6. Be Happy!
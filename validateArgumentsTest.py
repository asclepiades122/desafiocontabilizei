import unittest

import desafio

class TestReturnsFromAPI(unittest.TestCase):

    def test_all_states(self):
        result, filename = desafio.requestSearchCustomer('water', None)
        self.assertEqual(len(result), 131)

    def test_no_product(self):
        result, filename = desafio.requestSearchCustomer(None, None)
        self.assertEqual(result, [])

    def test_two_states(self):
        result, filename = desafio.requestSearchCustomer('ketchup', ['SP', 'MG'])
        print(len(result))
        self.assertEqual(len(result), 3)

if __name__ == '__main__':
    unittest.main()
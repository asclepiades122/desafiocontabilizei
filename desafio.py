#!/usr/bin/env python
# -*- coding: utf-8
import requests
import json
import sys
import validateArgumentsTest
from datetime import date

def fillFile(jsonToFile, firstId ,numberOfCompanies):
    today = date.today()
    filename = '/tmp/' + '{:02d}'.format(today.day) + '-' + '{:02d}'.format(today.month) + '-' + '{:02d}'.format(today.year) + '-' + firstId + '_' + str(numberOfCompanies) + '.csv'
    file = open(filename, "w")
    for company in jsonToFile:
        file.write('%s | %s | %f | %s \n' % (company['product'], company['id'], company['price'], company['states']))
    file.close()
    return filename

def getParameter():
    parameters = []
    countParameters=0
    for parametro in sys.argv:
        countParameters += 1
        parameters.append(parametro)
    return parameters

def requestSearchCustomer(productToSearch, statesToSearch):
    file = open("/tmp/lastSearch.txt", "w")
    file.write('%s|%s' %(productToSearch, json.dumps(statesToSearch)))
    file.close()
    url = "https://api-sandbox.contabilizei.com/ds/customers"
    r = requests.get(url)
    responseJson = json.loads(r.content)
    companiesResult = []
    for customer in responseJson:
        company = {}
        if statesToSearch != None:
            if(all(state in customer["company_state"] for state in statesToSearch)):
                found = False
                company["price"] = 0.0
                company["count"] = 0
                company["prices"] = []
                for productList in customer["Products_list"]:
                    if(productList["product"] == [productToSearch]):
                        found = True
                        company["product"] = str(productToSearch)
                        company["id"] = str(customer["company_Id"])
                        company["prices"].append(productList["product_price"])
                        company["price"] += float(productList["product_price"])
                        company["count"] += 1
                        company["states"] = ','.join(str(state) for state in customer["company_state"])
                if(found == True):
                    company["price"] = company["price"]/company["count"]
                    companiesResult.append(company)
        else:
            found = False
            company["price"] = 0.0
            company["count"] = 0
            company["prices"] = []
            for productList in customer["Products_list"]:
                if(productList["product"] == [productToSearch]):
                    found = True
                    company["product"] = str(productToSearch)
                    company["id"] = str(customer["company_Id"])
                    company["prices"].append(productList["product_price"])
                    company["price"] = company["price"] + float(productList["product_price"])
                    company["count"] = company["count"] + 1
                    company["states"] = ','.join(str(state) for state in customer["company_state"])
            if(found == True):
                    company["price"] = company["price"]/company["count"]
                    companiesResult.append(company)
    if(len(companiesResult) > 0):
        filename = fillFile(companiesResult, companiesResult[0]["id"], len(companiesResult))
    else:
        filename = None
    return companiesResult, filename

def main():
    parameters = getParameter()
    parameters[2] = parameters[2].split(',')
    code, msg = validateArgumentsTest.validateParameters(parameters)
    requestSearchCustomer(parameters[1], parameters[2])

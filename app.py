#!/usr/bin/env python
# -*- coding: utf-8
from flask import Flask, request
import desafio
import json

app = Flask(__name__)

states = ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO']

@app.route("/", methods=['GET'])
def hello_world():
    statesHtml = ''
    for state in states:
        statesHtml = statesHtml + '<input type="checkbox" name="states" value="%s">%s<br>' %(state, state)
    return """
    <button onclick="myFunction()">Última consulta realizada</button> <script> function myFunction() {  location.replace("http://127.0.0.1:5000/lastSearch")}</script>
    <form action="/searchCompanies" method='POST'>
    <p>Escolha o produto da companhia:</p>
    <input type="text" name="product" value="water">
    <p>Escolha os Estados onde atuam as empresas:</p>
    %s
    <input type="submit" value="Submit">
  </form>""" % statesHtml

@app.route("/searchCompanies", methods=['POST'])
def searchCompanies():
    productToSearch = request.form.getlist('product')
    print(productToSearch[0])
    statesToSearch = ','.join(str(state) for state in request.form.getlist('states'))
    statesToResponse = ' e '.join(str(state) for state in request.form.getlist('states'))
    statesToSearch = statesToSearch.split(',')
    if statesToSearch == ['']:
        statesToSearch = None
        statesToResponse = 'Todos os Estados'
    response, filename = desafio.requestSearchCustomer(productToSearch[0], statesToSearch)
    responseHtml = '%d empresas encontradas:' % len(response)
    for company in response:
        responseHtml = responseHtml + '<p>%s | %s | %f | %s</p>' %(company['product'], company['id'], company['price'], company['states'])
    if(filename != None):
        responseHtml = responseHtml + '<p> %s </p>' % filename
        responseHtml = responseHtml + '<p>Resultado da busca por "%s" em "%s"</p>' % (productToSearch[0], statesToResponse)
    responseHtml = responseHtml + '<button onclick="myFunction()">Nova busca</button> <script> function myFunction() {  location.replace("http://127.0.0.1:5000/")}</script>'
    return responseHtml

@app.route("/lastSearch", methods=['GET'])
def lastSearch():
    with open("/tmp/lastSearch.txt") as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    productToSearch = content[0].split('|')[0]
    statesToSearch = content[0].split('|')[1]
    if(json.loads(statesToSearch.replace("\'", "\"")) != None):
        print(json.loads(statesToSearch.replace("\'", "\"")))
        statesToResponse = ' e '.join(str(state) for state in json.loads(statesToSearch.replace("\'", "\"")))
    else:
        statesToResponse = 'Todos os Estados'
    response, filename = desafio.requestSearchCustomer(productToSearch, json.loads(statesToSearch.replace("\'", "\"")))
    responseHtml = '%d empresas encontradas:' % len(response)
    for company in response:
        responseHtml = responseHtml + '<p>%s | %s | %f | %s</p>' %(company['product'], company['id'], company['price'], company['states'])
    if(filename != None):
        responseHtml = responseHtml + '<p> %s </p>' % filename
        responseHtml = responseHtml + '<p>Resultado da busca por "%s" em "%s"</p>' % (productToSearch, statesToResponse)
    responseHtml = responseHtml + '<button onclick="myFunction()">Nova busca</button> <script> function myFunction() {  location.replace("http://127.0.0.1:5000/")}</script>'
    return responseHtml

app.run(port=5000, threaded=True, host=('0.0.0.0'))
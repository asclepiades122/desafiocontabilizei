FROM python
MAINTAINER Atila Freitas "atila.freitas.ti@gmail.com"

ADD . /app

WORKDIR /app

EXPOSE 5000

RUN pip install --upgrade -r requirements.txt

RUN python validateArgumentsTest.py

CMD ["python", "app.py"]
